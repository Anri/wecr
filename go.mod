module git.mylloon.fr/Anri/wecr

go 1.21.1

require github.com/joho/godotenv v1.5.1

require (
	github.com/hexops/gotextdiff v1.0.3
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
