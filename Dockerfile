# syntax=docker/dockerfile:1

FROM golang:1.21.1-alpine

WORKDIR /app
COPY . .

RUN go build -o wecr .

CMD ["./wecr"]
